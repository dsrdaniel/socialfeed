<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\FeedMessage
 *
 * @property integer $id
 * @property integer $feed_user_id
 * @property string $message
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\FeedMessage whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\FeedMessage whereFeedUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\FeedMessage whereMessage($value)
 * @method static \Illuminate\Database\Query\Builder|\App\FeedMessage whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\FeedMessage whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class FeedMessage extends Model
{
    //
}
