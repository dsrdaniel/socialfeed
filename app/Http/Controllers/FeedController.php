<?php

namespace App\Http\Controllers;

use App\FeedMessage;
use Illuminate\Http\Request;

class FeedController extends Controller
{
    /**
     * List messages
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $query = FeedMessage::query();

        $max_id = intval($request->get('max_id'));
        $min_id = intval($request->get('min_id'));

        $load_most_recent = $request->get('load_most_recent') == 'true';


        if($load_most_recent == true){

            $query->where('id', '>', $max_id);

        } else {

            $query->where('id', '<', $min_id);

        }

        /*
         * Limita o resultado somente quando é a primeira requisição dos mais novos,
         * ou quando está requisitando os mais antigos.
         */
        if(($load_most_recent && $max_id == 0) || !$load_most_recent){
            $query->limit(30);
        }

        $messages = $query->orderBy('id', 'desc')->get()->toArray();

        return response()->json($messages);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Save new message
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $feedMessage = new FeedMessage();

        $feedMessage->message = $request->get('message');
        $feedMessage->feed_user_id = $request->get('feed_user_id');

        $success = $feedMessage->save();

        response()->json(['success' => $success]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
