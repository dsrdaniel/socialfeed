<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\FeedUser
 *
 * @property integer $id
 * @property string $name
 * @property string $username
 * @property string $picture
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\FeedUser whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\FeedUser whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\FeedUser whereUsername($value)
 * @method static \Illuminate\Database\Query\Builder|\App\FeedUser wherePicture($value)
 * @method static \Illuminate\Database\Query\Builder|\App\FeedUser whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\FeedUser whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class FeedUser extends Model
{
    //
}
