var app = {};
$(function () {
    app = {

        //_host: "http://socialfeed.dev/api",
        _host: "/api",

        _user: {},

        _usersList: {},

        _intervalID: null,

        _messageList: {
            max_id: 0,
            min_id: 0
        },

        init: function () {


            this.loadUsers();

            this.initMessageEvents();

        },

        clearData: function () {

            this._user = {};

            clearInterval(this._intervalID);

            this._intervalID = null;

            this._messageList = {
                max_id: 0,
                min_id: 0
            };

        },

        initMessageEvents: function () {

            $("#btnEnviar").on('click', function () {

                var msg = $.trim($("#message_content").val());

                if (msg.length > 0) {

                    $("#btnEnviar").prop({disabled: true});

                    var data = {
                        message: $("#message_content").val(),
                        feed_user_id: app._user.id
                    }

                    $.ajax({
                        method: 'POST',
                        url: app._host + "/feed",
                        data: data
                    }).done(function () {

                        $("#message_content").val("");
                        $("#message_text_count").text(140);

                        app.loadMessageList(true);

                        $("#btnEnviar").prop({disabled: false});
                    });

                }


            });

            $("#message_content").on('keyup', function (e) {

                var textCounter = 140 - $("#message_content").val().length;

                if (textCounter == 140) {

                    $("#btnEnviar").prop({disabled: true});

                } else {

                    if ($("#btnEnviar").prop('disabled')) {

                        $("#btnEnviar").prop({disabled: false});
                    }

                }

                $("#message_text_count").text(textCounter);


            });

            $("#btnCarregarAntigas").on('click', function () {
                app.loadMessageList(false);
            });

        },

        unloadMessageList: function () {

            $("#messages_container").hide();
            $("#messages_list").html("");
            $("#feed_users_middle_container").fadeIn();

            app.clearData();

        },

        formatMessageListDates: function (messageData) {

            var currentDate = moment();

            $("#messages_list .date-message").each(function (i, value) {

                var messageDate = $(this).data('date');

                var created_at = moment(messageDate);
                var diffTime = currentDate.diff(created_at, 'seconds')
                var dateTmp = "";

                if (diffTime < 60) {
                    dateTmp = diffTime + "seg"
                }

                if (diffTime >= 60 && diffTime < 3600) {
                    dateTmp = currentDate.diff(created_at, 'minutes') + "min"
                }

                if (diffTime >= 3600 && diffTime < 86400) {
                    dateTmp = currentDate.diff(created_at, 'hours') + "hrs"
                }

                if (diffTime >= 86400) {
                    dateTmp = created_at.format('DD/MM/YY HH:mm');
                }

                $(this).html(dateTmp);

            });

        },

        loadMessageList: function (load_most_recent) {

            var self = this;

            if(app._intervalID != null){

                clearInterval(app._intervalID);

            }

            $("#feed_users_middle_container").fadeOut(function () {

                $("#messages_container").fadeIn();

            });

            load_most_recent = (typeof load_most_recent == "undefined" || load_most_recent == true) ? true : false;

            $.getJSON(this._host + "/feed", {
                feed_user_id: app._user.id,
                load_most_recent: load_most_recent,
                max_id: app._messageList.max_id,
                min_id: app._messageList.min_id
            }, function (response) {

                if (response.length > 0) {

                    if (app._messageList.min_id > 0) {
                        if (response[response.length - 1].id < app._messageList.min_id) {
                            app._messageList.min_id = response[response.length - 1].id;
                        }
                    } else {

                        app._messageList.min_id = response[response.length - 1].id;

                    }


                    if (load_most_recent) {

                        if (response[0].id > app._messageList.max_id) {
                            app._messageList.max_id = response[0].id;
                        }
                    }

                    $(response).each(function (i, value) {

                        $.extend(response[i], {
                            name: app._usersList[value.feed_user_id].name,
                            username: app._usersList[value.feed_user_id].username,
                            picture: app._usersList[value.feed_user_id].picture
                        });

                    });

                }

                var html = Handlebars.templates['message_list.hbs']({messages: response});

                if (load_most_recent) {

                    $("#messages_list").prepend(html);

                } else {

                    $("#messages_list").append(html);

                    if (response.length == 0) {

                        $("#btnCarregarAntigas").prop({disabled: true})

                    }

                }

                app.formatMessageListDates();

                app._intervalID = setInterval(function () {

                    self.loadMessageList(true);

                }, 3000);

            })

        },

        loadUsers: function () {

            $.getJSON(this._host + "/users").done(function (response) {

                var htmlOptions = '<option value="">Selecione um usuário</option>';

                $(response).each(function () {

                    app._usersList[this.id] = this;

                    htmlOptions
                        += "<option value=\"" + this.id + "\" data-content=\"<span class='media'><span class='media-left'><img src='" + this.picture + "' width='10' class='media-object'></span><span class='media-body'>" + this.name + "</span></span>\"></option>"

                });

                $("#feed_users_navbar, #feed_users_middle").html(htmlOptions);
                $("#feed_users_navbar, #feed_users_middle").selectpicker();

            }).fail(function (error) {

                alert("Não foi possível completar a operação.")

            });

            //EVENTS
            $("#feed_users_navbar, #feed_users_middle").on("changed.bs.select", function (e) {

                var val = $(e.target).val();

                var targetForceSelect = "feed_users_navbar";

                if ($(e.target).attr("id") == "feed_users_navbar") {

                    targetForceSelect = "feed_users_middle";

                }

                $("#" + targetForceSelect).selectpicker('val', val);

                app._user = app._usersList[val];

                if (typeof app._user != 'undefined' && typeof app._user.id != 'undefined') {

                    app.loadMessageList();


                } else {

                    app.unloadMessageList();

                }

            });

        }


    }

    app.init();
});
