<?php

use App\FeedMessage;
use App\FeedUser;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class FeedUsersTableSeeer extends Seeder
{

    /**
     * @var Carbon
     */
    private $_dt;

    /**
     * @var Faker\Factory
     */
    private $_faker;



    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $this->_faker = Faker\Factory::create();

        $this->_dt = Carbon::createFromDate(2016, 11,17);

        if(FeedUser::count() == 0){

            FeedUser::create([
                'name' => 'Daniel Ramos',
                'username' => 'danielramos',
                'picture' => '/img/people/m1.png',
            ]);
            FeedUser::create([
                'name' => 'João da Silva',
                'username' => 'joao_silva',
                'picture' => '/img/people/m2.png',
            ]);

            FeedUser::create([
                'name' => 'José Pereira',
                'username' => 'jose_pereira',
                'picture' => '/img/people/m3.png',
            ]);

            FeedUser::create([
                'name' => 'Ana Maria',
                'username' => 'ana_maria',
                'picture' => '/img/people/f1.png',
            ]);


            FeedUser::all()->each(function ($item, $key) {

                for ($i = 0; $i < 30; $i++) {

                    $this->_dt->addMinute();

                    FeedMessage::create(['feed_user_id'=>$item->id, 'message'=>$this->_faker->text(), 'created_at'=>$this->_dt, 'updated_at'=>$this->_dt]);

                }

            });

        }

    }
}
