<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeedMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('feed_messages', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('feed_user_id')->unsigned();
            $table->foreign('feed_user_id')
                ->references('id')->on('feed_users')
                ->onDelete('cascade');

            $table->string('message');

            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('feed_messages');
    }
}
