## Social Feed

Aplicação desenvolvida com Laravel Framework 5.3

https://laravel.com/docs/5.3

###Requisitos: https://laravel.com/docs/5.3#server-requirements

- PHP >= 5.6.4
- OpenSSL PHP Extension
- PDO PHP Extension
- Mbstring PHP Extension
- Tokenizer PHP Extension
- XML PHP Extension

## Instalação

As pastas storage e bootstrap/cache precisam ter permissão de escrita.

Excutar na raiz do projeto: 
 
 <code>composer install</code>

Alterar os dados de configuração do banco de dados no arquivo .env na raiz do projeto.

Banco de dados:

Pode ser importado o arquivo socialfeed.sql que está na raiz do projeto.

Ou através das migrations e populado pelos seeds:

Migration para criação da estrutura de dados: <br>
<code>
php artisan migrate
</code>


Seed para popular o banco com os usuários e mensagens iniciais: <br>
<code>
php artisan db:seed
</code>



## Observações

- Aplicação RESTful
- Stateless (não é utilizado sessão)
- Caso fosse utilizado autenticação, acredito que a utilização de OAuth2 ou JWT seria mais interessante
- ORM foi o Eloquent do próprio Laravel
- CORS com permissão para qualquer origem (para um hipotético uso da aplicação como web service) para o endpoint /api