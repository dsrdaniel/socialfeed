<!doctype html><!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]--><!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]--><!--[if IE 8]>
<html class="no-js lt-ie9" lang=""> <![endif]--><!--[if gt IE 8]><!-->
<html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="apple-touch-icon.png">

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <style>
        body {
            padding-top: 50px;
            padding-bottom: 20px;
        }
    </style>
    <link rel="stylesheet" href="css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="js/vendor/bootstrap-select/css/bootstrap-select.min.css">
    <link rel="stylesheet" href="css/main.css">

    <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
</head>
<body>


<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please
    <a href="http://browsehappy.com/">upgrade your browser</a>
    to improve your experience.
</p>
<![endif]-->

<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">SocialFeed</a>

        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <form class="navbar-form navbar-right" role="form">
                <div class="form-group">
                    <select id="feed_users_navbar">


                    </select>

                </div>

            </form>
        </div><!--/.navbar-collapse -->
    </div>
</nav>

<div class="container">

    <div class="row">

        <div class="col-md-3">

        </div>
        <div class="col-md-6" id="central_panel">

            <div class="panel panel-default text-center" id="feed_users_middle_container">

                    <div class="form-group">
                        <select id="feed_users_middle">


                        </select>

                    </div>

            </div>

            <div id="messages_container" style="display: none;">

                <div class="panel panel-default">
                    <div class="panel-body">
                        <form>
                            <div class="form-group">
                                <textarea class="form-control" rows="3" placeholder="O que está acontecendo?" id="message_content" maxlength="140"></textarea>
                            </div>
                            <button type="button" class="btn btn-default pull-right" id="btnEnviar" disabled>Enviar</button>
                                <span class="badge" id="message_text_count">140</span>
                        </form>
                    </div>
                </div>

                <ul class="list-group" id="messages_list">

                </ul>
                <button type="button" class="btn btn-default btn-lg btn-block" id="btnCarregarAntigas">Mais antigas</button>

            </div>
        </div>
        <div class="col-md-3">

        </div>

    </div>


</div> <!-- /container -->

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

<script src="js/vendor/bootstrap.min.js"></script>
<script src="js/vendor/bootstrap-select/js/bootstrap-select.min.js"></script>

<script src="js/vendor/handlebars.runtime.min.js"></script>

<script src="js/templates/message_list.js"></script>

<script src="js/vendor/moment.min.js"></script>

<script src="js/main.js"></script>
</body>
</html>
